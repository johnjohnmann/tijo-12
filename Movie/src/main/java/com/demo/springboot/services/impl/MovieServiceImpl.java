package com.demo.springboot.services.impl;

import com.demo.springboot.domain.dto.MovieDto;
import com.demo.springboot.services.MovieService;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import org.springframework.stereotype.Service;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class MovieServiceImpl implements MovieService {

    public List<MovieDto> readMovies() {
        List<MovieDto> movies = new ArrayList<>();

        CSVParser parser = new CSVParserBuilder()
                .withSeparator(';')
                .withIgnoreQuotations(true)
                .build();

        try (CSVReader csvReader = new CSVReaderBuilder(new FileReader("src/main/resources/movies.csv"))
                .withSkipLines(1)
                .withCSVParser(parser)
                .build()) {

            String[] line;
            while ((line = csvReader.readNext()) != null) {
                movies.add(new MovieDto(
                        Integer.parseInt(line[0].trim()),
                        line[1].trim(),
                        Integer.parseInt(line[2].trim()),
                        line[3].trim()
                ));
            }
        } catch (IOException error) {
            System.out.println("IOException!");
        }

        return movies;
    }
}
